json.extract! song, :id, :name, :date_of_release, :songwriter, :melodywriter, :created_at, :updated_at
json.url song_url(song, format: :json)
