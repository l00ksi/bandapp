json.extract! concert, :id, :name, :location, :start_time, :end_time, :ticket_price, :capacity, :created_at, :updated_at
json.url concert_url(concert, format: :json)
