json.extract! ticket, :id, :name, :concert_id, :price, :created_at, :updated_at
json.url ticket_url(ticket, format: :json)
