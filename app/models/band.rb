class Band < ApplicationRecord
	has_one_attached :avatar
	has_many :members
	has_many :albums
	belongs_to :city
	has_many :concert_performers
	has_many :concerts, through: :concert_performers
end
