class Concert < ApplicationRecord
	has_many :concert_performers
	has_many :bands, through: :concert_performers
	has_many :tickets
	belongs_to :city
end
