class City < ApplicationRecord
	validates :name, uniqueness: true
	validates_presence_of :name
	has_many :concerts
	has_many :bands
end
