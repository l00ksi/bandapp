class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.string :name
      t.integer :concert_id
      t.float :price

      t.timestamps
    end
  end
end
