class CreateConcertPerformers < ActiveRecord::Migration[5.2]
  def change
    create_table :concert_performers do |t|
      t.integer :concert_id
      t.integer :band_id

      t.timestamps
    end
  end
end
