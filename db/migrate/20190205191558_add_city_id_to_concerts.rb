class AddCityIdToConcerts < ActiveRecord::Migration[5.2]
  def change
    add_column :concerts, :city_id, :integer
  end
end
