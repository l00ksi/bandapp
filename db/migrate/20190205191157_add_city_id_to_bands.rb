class AddCityIdToBands < ActiveRecord::Migration[5.2]
  def change
    add_column :bands, :city_id, :integer
  end
end
