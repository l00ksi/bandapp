class CreateSongs < ActiveRecord::Migration[5.2]
  def change
    create_table :songs do |t|
      t.string :name
      t.date :date_of_release
      t.string :songwriter
      t.string :melodywriter

      t.timestamps
    end
  end
end
