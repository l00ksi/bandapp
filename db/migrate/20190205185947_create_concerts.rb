class CreateConcerts < ActiveRecord::Migration[5.2]
  def change
    create_table :concerts do |t|
      t.string :name
      t.string :location
      t.datetime :start_time
      t.datetime :end_time
      t.float :ticket_price
      t.integer :capacity

      t.timestamps
    end
  end
end
