# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


cities = ["Zagreb", "Split", "Rijeka", "Osijek", "Zadar", "Pula", "Sesvete", "Slavonski Brod", "Karlovac", "Varaždin", "Šibenik", "Sisak", "Vinkovci",
 "Velika Gorica", "Dubrovnik", "Bjelovar", "Vukovar", "Koprivnica", "Solin", "Zaprešić", "Požega", "Đakovo", "Samobor", "Petrinja", "Metković", "Čakovec",
  "Virovitica", "Kutina", "Makarska", "Rovinj - Rovigno", "Županja", "Nova Gradiška", "Sinj", "Križevci", "Trogir", "Knin", "Dugo Selo", "Kastav", "Slatina", 
  "Poreč - Parenzo", "Čepin", "Ivanić-Grad", "Podstrana", "Daruvar", "Ogulin", "Beli Manastir", "Našice", "Valpovo", "Tenja", "Umag - Umago", "Kaštel Stari", 
  "Novska", "Labin", "Crikvenica", "Kaštel Sućurac", "Vodice", "Višnjevac", "Opatija", "Gospić", "Belišće", "Omiš", "Kaštel Novi", "Đurđevac", "Donji Miholjac",
   "Ivankovo", "Mali Lošinj", "Nova Mokošica", "Ploče", "Duga Resa", "Pitomača", "Biograd na Moru", "Jastrebarsko", "Kaštel Lukšić", "Darda", "Ivanec", "Ilok", 
   "Borovo", "Kaštel Kambelovac"]

cities.each do |city|
	City.create(:name => city)
end