Rails.application.routes.draw do
  devise_for :users
  get 'welcome/index'
  resources :cities
  resources :tickets
  resources :concerts
  resources :albums
  resources :songs
  resources :members
  resources :bands

  root 'welcome#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
